const search_link = document.querySelector('.header-menu__search-icon a');
const search_form = document.querySelector('.header-menu__search-form');
const search_form_input = document.querySelector('.header-menu__search-form input');
const search_icon = document.querySelector('.header-menu__search-icon');

search_link.addEventListener('click',function(e){
    e.preventDefault();
    search_form.classList.toggle('search-form-show');
    search_icon.classList.toggle('header-menu__search-icon--close');
    search_form_input.focus();
});